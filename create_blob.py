# uses a local 'assets.sql' with 'file' and 'reference' for every table
# corresponding to the entries in blobs (line 12)

import os
import sqlite3
import datetime

database = "assets.sql"
blob_header_text = "This is an asset blob. Why are you so interested in it?"
blob_header = bytearray(blob_header_text, 'utf8')

blobs = ["background", "portrait", "tile", "entity", "world", "misc", "debug"]
asset_list_format = "const int {}_{} = {};\n"

conn = sqlite3.connect(database)
c = conn.cursor()

for blobtype in blobs:
    output = "_blobs/{}_assets.blob".format(blobtype)
    size_file = "_blobs/{}_assets_sizes".format(blobtype)
    asset_list_file = "_blobs/assets_{}.hpp".format(blobtype)

    size_table = list()
    database_read = c.execute("SELECT file, reference FROM {}".format(blobtype))  # it's not like i'm going to inject myself

    try:
        #create the blob
        fetched = database_read.fetchone()
        filename = "{}/".format(blobtype) + fetched[0]
        asset_name = fetched[1]
    except TypeError:
        continue  # asset database has nothing of this blob type

    asset_num = 1
    with open (asset_list_file, "w") as asset_list_f:
        asset_list_f.write("/*\n * assets_{}.hpp\n *\n * AUTOGENERATED ({})\n */\n\n#pragma once\n\nnamespace assets {{\n".format(blobtype, datetime.datetime.now().strftime("%Y-%m-%d %H:%M")))
        with open (size_file, "w") as size_f:
            with open(output, 'wb') as fo:  # 'ab' is binary append
                fo.write(blob_header)
                size_table.append(len(blob_header))
                while not filename is None:
                    with open(filename, 'rb') as fi:
                        data = fi.read()
                        
                        asset_list_f.write(asset_list_format.format(blobtype.upper(), asset_name.upper(), asset_num))
                        
                        asset_num += 1
                        size_table.append(len(data)+size_table[-1])
                        fo.write(data)
                        data = None
                        try:
                                    fetched = database_read.fetchone()
                                    filename = "{}/".format(blobtype) + fetched[0]
                                    asset_name = fetched[1]
                        except TypeError:
                            break  # NoneType is not subscriptable
            for x in size_table:
                size_f.write(str(x)+"\n")
        asset_list_f.write("}  // namespace assets")